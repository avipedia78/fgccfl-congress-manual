\section{Floor Debate: Speeches and Questions}

\subsection{Floor Debate Generally}\label{fd}

\subsubsection{}\label{fd_timelimit}
There is no debate time limit at local tournaments. However, the chamber is
expected to consider \textbf{at least} half the items on its Primary Agenda
in each session. This may necessitate ordering the previous question or
tabling items even though some legislators still wish to speak.
\emph{REMEMBER: No person is entitled to give a speech on a bill or
resolution other than the author.}

\subsubsection{}\label{fd_recog}
A legislator who wishes to speak on legislation must wait until the PO calls
for speakers before rising.\footnote{\fnrise} Once recognized, he may proceed
to the rostrum or other place designated for speeches and shall state his
school code and name for the judges.

\subsubsection{}\label{fd_side}
Legislators may rise only to oppose the views of the previous speaker.
\emph{However, if no one wishes to oppose the previous speaker, 
\textbf{the speaking opportunity is forfeited}, and the PO recognizes another
speaker on the same side.} If there are no speakers on either side, the PO
should ask if the chamber is \emph{``ready for the question''}; if no one
then seeks the floor, she may take a passage vote.

\subsubsection{}\label{fd_bothsides}
Legislators are not prohibited \emph{by rule} from speaking on both sides of
a topic. However, the judges may deem this practice unethical when
scoring/ranking (absent an amendment to the legislation or
another compelling reason to switch sides), and the PO may consider whether
a speaker who has changed positions is attempting to monopolize debate.

\subsubsection{}\label{fd_visualaids}
Non-electronic visual aids, \emph{e.g.}, display charts, are permitted 
but must be made available for use by other speakers. Properties and 
costumes are prohibited.

\subsubsection{}\label{fd_gaveling}
Speeches are limited to 3 minutes; speakers \textbf{may not} reserve or yield
time. The PO shall time all speakers, gaveling \textbf{once at 2 minutes} and
\textbf{twice at 2\nicefrac{1}{2} minutes}. At \textbf{3 minutes}, the PO
shall allow the speaker to complete his sentence, then
\textbf{gavel down the speaker} and ask him to yield the floor.
There is no grace period or ``warning tap.''
The PO shall announce the duration of each speech.


\subsection{Authorship and Sponsorship Speeches}\label{auth}

\subsubsection{}\label{auth_author}
The author of a bill or resolution (B/R), as designated on the calendar, is
entitled to give the first speech on the B/R (\emph{authorship speech}),
regardless of any other priority considerations.
This privilege belongs to the author exclusively, 
\textbf{not} to other legislators from the author's school.

\subsubsection{}\label{auth_sponsor}
If the author (other than the sitting PO) is unwilling or unable to speak
when the B/R comes up for debate, or if the B/R does not carry a sponsor's name 
on the calendar, the PO shall select a sponsor from the chamber at large
to deliver a \emph{sponsorship speech}, which shall be considered
equivalent to an authorship speech for procedural purposes.\footnote{\fnsponsor}

\subsubsection{}\label{auth_pobill}
If the PO's legislation comes up for debate, she may entertain a motion to
table or postpone it, or she may allow it to be treated as unsponsored.

\subsubsection{}\label{auth_nosponsor}
If there is no sponsor, the chamber should lay the B/R on the table until
a sponsor is available. If the chamber exhausts the rest of the agenda, the
B/R may be taken from the table and the previous question ordered immediately.


\subsection{Priority System}\label{pri}

\subsubsection{}\label{pri_prisys}
The PO \textbf{must} recognize all affirmative, negative,
and sponsorship speakers as follows:
\begin{enumerate}[noitemsep]
  \item\label{prisys_numspeech}\textbf{Number of speeches (precedence).}
    The PO recognizes a legislator who has \emph{not yet spoken} in the session;
    if all who wish to speak have spoken, she recognizes the one who has given
    the \emph{fewest} speeches in the session.
  \item\label{prisys_recency}\textbf{Recency.}
    If there is a tie for number of speeches, the PO recognizes the
    legislator whose \emph{most recent} speech was given
    \emph{earliest} in the session.
  \item\label{prisys_discretion}\textbf{PO's discretion.}
    If multiple legislators rise who have not yet spoken, 
    the PO selects a speaker at her discretion, 
    keeping in mind her obligation to be \emph{scrupulously fair}. 
    The PO \textbf{must not} consider activity
    in the chamber, ``standing time,'' or school affiliation in her
    selection process. (In the Super Congress or at the discretion 
    of the Congress Coordinator, a randomly generated priority 
    list may substitute for the PO's discretion.)
\end{enumerate}

\subsubsection{}\label{pri_speechcount}
Only speeches on legislation and amendments count against priority.
A speech is recorded the moment it begins, even if the speaker is later
ruled \ooo. Questions, motions, and speeches on motions (except
amendments to B/R) are not recorded.
\emph{Each session begins with a clean slate (zero speeches).}

\subsubsection{}\label{pri_records}
The PO is responsible for maintaining accurate records of priority.
She must make each recognition decision as it is required;
she \textbf{may not} pre-plan selections because new people may rise each time.
\emph{Systematic errors or bias in recognition, if raised and not corrected,
is a sufficient basis to reduce the PO's ``procedure'' score to the minimum.}

\subsubsection{}\label{pri_error}
An error in recognition may only be challenged on an \textbf{\emph{immediate}}
point of order.\footnote{\fnprierror} In making future selections, the PO
\textbf{must not} disregard the priority system to compensate for the error.

\subsubsection{}\label{pri_gaveldown}
A speaker who is \ooo\ (wrong side, wrong topic, incivility) should be
gaveled down, subject to appeal, once the PO is \emph{certain} that the
speaker is in the wrong. Members who believe that a speaker is \ooo\
\textbf{may} interrupt the speech to rise to a point of order.

\vspace{2em}

\subsection{Cross-Examination (Questioning Periods)}\label{cx}

\subsubsection{}\label{cx_times}
Following an authorship speech and the first negative speaking opportunity
on an item of legislation,
the author/sponsor and first negative speaker\footnote{\fnfirstneg} 
\textbf{must} answer questions
for 2 minutes. All other speakers \textbf{must} answer questions for 1
minute following their speeches. The chamber \textbf{may not} suspend the
rules to extend questioning, to alter or abolish regular questioning periods,
or to adopt ``direct questioning.''

\subsubsection{}\label{cx_recognition}
The PO recognizes questioners, but the chamber \emph{may} suspend the rules to
permit speakers to do so themselves. There is no priority system for questioners
(and the PO \textbf{should not} attempt to create or enforce one), but no
legislator should be allowed to monopolize the floor. The PO has the power to
cut off a questioner or speaker and select a new questioner (or to order the
speaker to do so).

At the end of the questioning period, the PO should rise and,
upon the completion of a final brief response, tap her gavel once to signal
the end of the questioning period. If there is a lack of questioners with 
time remaining, the PO should rise and end the questioning period early.

\subsubsection{}\label{cx_procedure}
Once recognized, the questioner may ask a \emph{one sentence} question, 
including brief prefatory information (there is no ``permission to preface''),
then takes his seat. The speaker gives a \emph{brief} response (but \textbf{must not}
be limited to an unqualified ``yes'' or ``no''), and the next questioner is
recognized. Clarifications are permitted at the PO's discretion. The speaker
may refuse to answer a ``loaded'' question or any other question offered
in bad faith.

\subsubsection{}\label{cx_excessive}
The PO \textbf{must} cut off excessive commentary or oration by the speaker or
the questioner. She should be especially careful with ``Are you aware~\dots~/ Do you
agree~\dots~/ Isn't it true~\dots~?'' questions and with ``friendly'' questions of any
sort, as these are often used as vehicles for extended oration.

\subsubsection{}\label{cx_prohibited}
Personal attacks, repetitive or irrelevant questions, extended prefacing,
serial questions, and requests to engage in prohibited conduct \textbf{must}
be ruled \ooo. Cross-debate, in which the speaker demands a response from
the questioner, is not permitted. (A rhetorical question does not constitute 
cross-debate.)
