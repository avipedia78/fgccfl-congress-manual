\section{Parliamentary Procedure}

\subsection{Parliamentary Motions Generally}\label{mot}

\subsubsection{}\label{mot_philosophy}
Parliamentary procedure is essential to Student Congress. However, time spent
on motions might otherwise be used for substantive debate, so procedure should
not be used unnecessarily. \emph{Parliamentary games and other abuses will be
penalized by the officials.}

\subsubsection{}\label{mot_dilatory}
The PO \textbf{must} rule \ooo\ all motions she deems dilatory (intended to
waste time) or frivolous (lacking serious purpose), subject to appeal.
The PO \textbf{may not} reject an appeal or a call for a division, but the
officials may intervene if these checks on the PO's power are abused.

\subsubsection{}\label{mot_requiredspeeches}
If the named author is willing and able to speak when his legislation comes to
the floor, there must be an authorship speech and an opportunity for a 
first negative speech before the legislation is voted on or set aside.
Otherwise, there are no speech or cycle requirements.

\subsubsection{}\label{mot_precedence}
The order of precedence is reflected in the Table of Parliamentary Motions.
The chamber disposes of one motion at a time, from highest to lowest precedence.

\subsubsection{}\label{mot_process}
The particular rules governing each motion appear in the Table of Parliamentary
Motions or in \emph{Robert's Rules of order, Newly Revised}. This is a general
process:
\begin{enumerate}
  \item\label{motproc_recognition}\textbf{Recognition.}
    A legislator may rise for a motion at any time except during a
    speech.\footnote{\fninterrupt} If necessary, the legislator may speak out
    (\emph{e.g.}, \emph{``Madam/Mister Speaker (President)!''} or \emph{``Motion!''})
    to attract the PO's attention. A motion that must be made
    \textbf{\emph{immediately}} is only in order until the next speech begins
    or the pending motion is voted or ruled upon. (The PO \textbf{should not}
    call for motions between speeches; it is legislators' job to seek
    recognition.)
  \item\label{motproc_statement}\textbf{Statement of the motion.}
    The PO recognizes the speaker and asks him to state his point. The
    legislator then states motion using the appropriate
    wording.\footnote{\fnwording} The PO determines whether the motion is
    in order.
  \item\label{motproc_second}\textbf{Second.}
    If a second is required, another legislator must support the motion
    (\emph{``Second!''}) for the motion to be considered. If necessary,
    the PO will call for a second from the chamber. (By rule, legislation
    on the calendar is presumed to have been seconded.)
  \item\label{motproc_debate}\textbf{Debate and amendment (if permitted).}
    Debatable motions may be discussed pro and con. Except when the chamber
    is debating an amendment to legislation, remarks on motions are delivered
    from members' desks (as always, speakers should stand), should not
    exceed 30 seconds, are not scored, and do not count against priority.
    No legislator should be permitted to monopolize the floor. Amendable
    motions may be amended with a motion to amend, a second, and a majority
    vote; the motion to amend may be debated only if the motion being amended
    is itself debatable.
  \item\label{motproc_outcome}\textbf{Vote or decision.}
    Once debate (if any) is exhausted or closed, the appropriate vote is taken.
    Some incidental and privileged motions are decided by the PO, without any
    vote being taken; these decisions are appealable.
\end{enumerate}

\subsection{Privileged Motions}\label{priv}
The privileged motions concern the immediate needs of the chamber and its
members. Because of their immediate nature, they take priority over all other
matters and must be resolved quickly. They are listed here in order of precedence,
from highest to lowest.

\subsubsection{Adjourn (including \emph{sine die}).}\label{priv_adjourn}
A motion to adjourn ends the session and dismisses the chamber until the
time specified. By rule, it does not affect tabled items but does terminate
any suspensions of the rules. It is not necessary to ``reopen the floor''
upon returning from an adjournment.

A motion to adjourn \emph{sine die}\footnote{Latin for ``without day,''
pronounced \emph{SIGH-nee-DIE-ee}.} ends the session without providing
for the chamber's return, effectively defeating any items tabled or not
considered. It is only used at the end of the last session.

\subsubsection{Recess.}\label{priv_recess}
A motion to recess allows the chamber to suspend all business for a specified
period or until a set time without ending the session. By rule, recesses
count against session time. It is not necessary to ``reopen the floor''
upon returning from a recess. 
(\textbf{NOTE:} By rule, use of the motion to recess is restricted at 
FGCCFL tournaments. See \S~\ref{sess}\ref{sess_recess}.)

\subsubsection{Point of personal privilege.}\label{priv_personal}
This motion allows a legislator to make a personal request during debate
(most often to exit or enter the chamber). It \textbf{may not} be used to
address or inquire of the chamber or to approach the Chair (except to
approach with an amendment).

\subsubsection{Call for the orders of the day.}\label{priv_orders}
This motion compels the chamber to take up scheduled business (so-called
general or special orders, created either by rule or by certain motions).
It is only required if the PO does not otherwise proceed to the orders of the
day at the established time. All other debate is suspended while the chamber
considers the orders of the day.


\subsection{Incidental Motions}\label{inci}
The incidental motions concern the rules and conduct of debate in the chamber.
Only the motions most likely to be used in Student Congress are described in
this section. They are listed here in order of convenience
because they have no order of precedence among themselves.

\subsubsection{Point of order.}\label{inci_order}
A legislator may rise to a point of order to correct a procedural error that
affects the interests of individual members or the chamber. 
It \textbf{must not} be used simply to demonstrate the legislator's procedural
knowledge (or the PO's lack thereof). 
Withrespect to errors in speaker recognition, see \S~\ref{pri}\ref{pri_error}.

The motion must be made \textbf{\emph{immediately}}, interrupting the PO
(\emph{``Point of order!''}) if necessary. Upon being asked to state his
point, the legislator explains how the issue should have been decided (citing
relevant rules), then takes his seat. The PO then determines whether the
point of order is \emph{``well taken''} (correct) and either takes corrective
action or explains the basis for her ruling.

\subsubsection{Appeal.}\label{inci_appeal}
If a point of order is not resolved to legislators' satisfaction, they may
appeal the PO's decision to the full chamber. A motion to appeal (phrased
\emph{``I appeal the decision of the Chair.''}) must be made
\textbf{\emph{immediately}}, interrupting the PO 
(\emph{``Madam/Mister Speaker (President)!''})
if necessary. It requires a second and is not debatable, though the
legislator making the motion and the PO may \emph{briefly} state the basis for
reversing or affirming the ruling.

The PO takes a standing vote on the question \emph{``Shall the decision of the
Chair stand?''} Those voting to affirm the Chair are counted first, followed by
those voting to reverse. A tie vote affirms the ruling. Once the results of the
appeal are announced, the question may no longer be debated in the chamber.

\subsubsection{Suspension of the rules.}\label{inci_suspendrules}
A motion to suspend the rules must specify the limited purpose for which it was
made, \emph{e.g.}, adopting a hand vote or allowing speakers to recognize
their own questioners. Suspensions are permitted only for matters either
authorized by this manual or not addressed by the FGCCFL and NCFL rules.
A motion to suspend the rules requires a second and a \nicefrac{2}{3} vote
of the members \emph{present}. It expires when the chamber adjourns.

\subsubsection{Call for a division of the chamber.}\label{inci_division}
This is a motion to contest a voice vote. It must be made
\textbf{\emph{immediately}} following the disputed vote, interrupting
the PO (\emph{``Division!''}) if necessary. A second is required (this
is an NCFL rule and differs from \emph{Robert's}), but no vote
is taken on the call; instead, the PO immediately takes a standing
vote on the question.



\subsection{Subsidiary Motions}\label{subs}
The subsidiary motions, including amendments (\S~\ref{amend}), affect the B/R
on the floor for consideration. Only the motions most likely to be used in
FGCCFL Student Congress are described in this section. They are listed here in
order of precedence, from highest to lowest.

\subsubsection{Lay on (or take from) the table.}\label{subs_table}
The motion to lay a B/R on the table (``table the B/R'') sets the legislation
aside \emph{with the intention of returning to it later}. When no B/R is on
the floor, the chamber may vote to take the item from the table and resume
debate where it left off. These are majority questions.

\subsubsection{The previous question.}\label{subs_pq}
The previous question is a motion to close debate and vote immediately on the
item being debated. Its proper phrasing is \emph{``I move the previous
question.''}\footnote{\fnpq} It requires a second and a \nicefrac{2}{3} vote
of those \emph{present}.

\textbf{Legislators should not overwork this motion, but neither should they
neglect it.} As long as speakers are advancing debate, it is proper to hear
them out. However, when debate grows stale or one-sided, it is usually
in legislators' best interests to move on to a fresh topic.

\subsubsection{Limit (or extend the limits of) debate.}\label{subs_limit}
This motion allows debate on a B/R to be limited to a number of minutes or
speeches, or to end at a certain time. Because it affects freedom of debate,
it requires a second and a \nicefrac{2}{3} vote of those \emph{present}.
It is not debatable, but it is amendable as to time. This motion
\textbf{may not} be used to exceed any maximum debate time imposed by rule 
(as may be the case at Grand Finals).

\subsubsection{Postpone to a specific time.} This motion allows the chamber to
set a B/R aside until after a specified time, \emph{e.g.}, postponing the
Session 1 PO's bill until the start time of Session 2. This is a majority
question and requires a second. Once the specified time has come and no other
item is on the floor, debate on the postponed B/R continues where it left off
(if the PO forgets, see \S~\ref{priv}\ref{priv_orders}).


\subsection{Amending Process for Legislation}\label{amend}

\subsubsection{}\label{amend_definition}
An amendment \textbf{must} be germane to the legislation's subject matter and
\textbf{must not} alter the legislation's intent. The amendment is written as
a single imperative sentence directing \textbf{one} of the following changes:
\begin{itemize}[noitemsep]
  \item \textbf{Strike out} one or more occurences of a word/phrase/passage
  \item \textbf{Insert} one or more occurrences of a word/phrase/passage
  \item \textbf{Substitute} new material for part or all of the existing text
\end{itemize}
(It is presumed that any required renumbering of sections or lines will be
carried out without further instruction.) The written amendment must also
specify the B/R title (or number), the name of the amendment's author, and
the line numbers of the affected lines.

An amendment to legislation may be amended using the same procedure, but
an amendment \emph{to an amendment} \textbf{may not} be amended further.

\subsubsection{}\label{amend_review}
The amendment must be written out and be submitted to the PO (at All Events
tournaments) or Parliamentarian (at Grand Finals), who will ensure that the
amendment satisfies the requirements above. A legislator \textbf{may} rise
to a point of personal privilege to approach with an amendment.

\subsubsection{}\label{amend_motion}
While the B/R is on the floor, the author of the amendment should seek
recognition \emph{for a motion} and move to amend the legislation. The PO
will confirm that the amendment is satisfatory, read the amendment to the
chamber, and call for a second by standing vote. One-third (\nicefrac{1}{3})
of the members \emph{present} must vote to second the amendment.

\subsubsection{}\label{amend_debate}
If the amendment receives a \nicefrac{1}{3} second, it \textbf{may} be
debated. However, the chamber may instead choose to order the previous
question immediately and vote on the amendment without debating it.
Debate, if any, begins with an \emph{affirmative speech}, assigned
according to the rules of priority. The author of the amendment
\textbf{is not} automatically entitled to open debate.

\subsubsection{}\label{amend_speeches}
Speeches on amendments alternate pro and con, are given from the rostrum,
are 3 minutes in duration with a 1 minute questioning period, are scored,
and are subject to the priority rules. However, debate is limited to the
amendment; speeches on the merits of the underlying legislation are \ooo\
while the amendment is on the floor.

\subsubsection{}\label{amend_vote}
When debate on the amendment has been exhausted or the previous question
has been ordered, a standing vote is taken on the adoption of the amendment.
An amendment requires a simple majority. Following the vote, debate on the
legislation (or the legislation \emph{as amended}) continues where it left off.


\subsection{Voting Procedures: Standing Votes}\label{vorec}

\subsubsection{}\label{vorec_when}
A standing (or ``recorded'') vote is required for the following:
\begin{enumerate}[noitemsep]
  \item Passage of a B/R
  \item Adoption of an amendment to a B/R
  \item An appeal of the Chair's decision
  \item Any question requiring a \nicefrac{2}{3}, \nicefrac{1}{3},
    or \nicefrac{1}{5} vote
  \item Any question on which a division of the chamber
    (\S\S~\ref{inci}\ref{inci_division}~\& \ref{vovo}\ref{vovo_division})
    has been ordered
\end{enumerate}
The PO \textbf{may} additionally choose to take a standing vote in place of
a voice vote on any majority procedural question (this is recommended if
the PO believes the vote will be close).

\subsubsection{}\label{vorec_hands}
The chamber may suspend the rules to substitute a show of hands for
all standing votes.\footnote{\fnhands}
By rule, this substitution is automatic if the
chamber is using raised hands for recognition.

\subsubsection{}\label{vorec_method}
To take a standing vote, the PO states the question under consideration and
the vote required for passage. She then calls for those in favor to rise
(or raise their hands) and counts the votes. For majority questions only,
she then calls for those opposed to do likewise.  The PO \textbf{never}
calls for abstentions.\footnote{\fnquorum}

\subsubsection{}\label{vorec_majority}
A motion requiring a \emph{simple majority} passes if and only if there
are more votes in favor (``ayes'') than in opposition (``noes''). A tie
vote defeats the question. The PO \textbf{may} vote \emph{if her vote will
affect the outcome}---either an ``aye'' vote that breaks a tie or a ``no''
vote that creates one.

The PO announces the results as follows: \emph{``By a vote of \bblank\
in favor to \bblank\ opposed, the motion (bill, resolution, amendment,
etc.) is adopted/defeated.''}

\subsubsection{}\label{vorec_fraction}
Ordering the previous question, suspending the rules, seconding an
amendment, and other non-majority questions are based on the \emph{fraction of
legislators present}, not including the PO. If the required
fraction of members votes in favor of the question, it passes; otherwise,
it fails.

When the result is announced, the vote is given as 
\emph{``\dots~\bblank\ in favor out of \bblank\ present~\dots.''}

\subsubsection{}\label{vorec_rollcall}
A roll call vote may be ordered on a question 
by \nicefrac{1}{5} of the members \emph{present}. 
\textbf{\emph{This is strongly discouraged unless there have been 
irregularities in a standing vote count.}} 
The PO calls the roll from her seating chart, and each member 
responds by saying \emph{only} ``Aye'' or ``No''; a legislator who 
does not wish to vote need not say anything, but if pressed, he 
may abstain by saying ``Present.'' Members who do not respond 
are called a second time. The vote is announced as usual.

\subsection{Voting Procedures: Voice Votes}\label{vovo}

\subsubsection{}\label{vovo_allowed}
A voice vote may be taken \emph{only} on those questions for which a
standing vote is not required, \emph{i.e.,} questions that require a
simple majority and that are not passage votes or appeals. However,
the PO may choose to take a standing vote on any or all of these questions.

NOTE: Even when a motion will obviously pass, a vote must be taken.
The PO \textbf{may not} simply call for objections, nor may she
``assume unanimous consent.''

\subsubsection{}\label{vovo_method}
To take a voice vote, the PO states the question under consideration.
She calls first for those in favor to say \textbf{``aye,''} then for
those opposed to say \textbf{``no.''}\footnote{\emph{Not} ``nay,''
unless they're horses.}

The PO determines the result based upon her judgment and announces the
outcome as follows: \emph{``In the opinion of the Chair, the ayes/noes
have it, and the motion passes/fails.''} If the PO cannot determine
the outcome, she should announce that there is a division of the chamber
and immediately take a standing vote (\S~\ref{vorec}).

\subsubsection{}\label{vovo_division}
If legislators disagree with the PO's ruling on a voice vote, they may
\textbf{\emph{immediately}} call for a division of the chamber; see
\S~\ref{inci}\ref{inci_division}.











% Fin.
