\section{Tournament Operations}

\subsection{Submission and Review of Legislation}\label{reg}

\subsubsection{}\label{reg_quota}
Schools with Congress entries \textbf{must} submit at least one satisfactory
item of legislation. Schools may submit a maximum of six
items, but no more than one item per legislator.

\subsubsection{}\label{reg_deadline}
This is the timeline for review of legislation
(late items \textbf{will not} be considered under any circumstances):
\begin{center}
  \begin{tabular}{lp{3.75in}}
    \hline
    \parbox[t]{1.75in}
      {\textbf{9:00 P.M. Monday}\\\footnotesize 12 days before tournament} &
      Deadline for coaches to upload legislation to Tabroom.com for review 
      (\emph{recommended but not required}) \\
    \hline
    \parbox[t]{1.75in}
      {\textbf{9:00 P.M. Thursday}\\\footnotesize 9 days before tournament} &
      Final deadline for coaches to upload legislation to Tabroom.com
      for consideration \\
    \hline
    \parbox[t]{1.75in}
      {\textbf{9:00 P.M. Friday}\\\footnotesize 8 days before tournament} &
      Legislation packet (docket) posted on Tabroom.com and on the
      Congress site \\
    \hline
  \end{tabular}
\end{center}

\subsubsection{}\label{reg_upload}
\textbf{Coaches must personally review and submit all legislation.}
Legislation must be uploaded to Tabroom.com in Microsoft Word format, one B/R
per document. Legislation in an incorrect file format will not be reviewed,
and pages after the first will be ignored. Submissions may be modified or
replaced up to the Thursday deadline, at which time they will be reviewed and
approved or rejected.

\subsubsection{}\label{reg_review}
Coaches who wish to have their students' legislation reviewed in advance must
upload it to the Tabroom.com no later than the Monday prior to
tournament week. \textbf{Advance review is highly recommended but not required.}
If modifications are required, the final legislation must be uploaded 
to Tabroom.com by the Thursday deadline.

\subsubsection{}\label{reg_reject}
Legislation will be rejected if it is frivolous, stale, recycled, deficient,
or otherwise unsatisfactory.
In questionable cases, the Congress Coordinator
may revise the legislation (in a manner that does not alter its intent) and
approve the revision. In all cases, the Congress Coordinator may make
technical corrections or modify titles.

(NOTE: Legislation will not be rejected solely because it is unconstitutional,
unworkable, or ill-advised. These are legitimate issues to be raised in floor
debate. \emph{The inclusion of an item of legislation on the docket
\textbf{does not} constitute a ``clean bill of health'' from the Congress
Coordinator!})

\subsubsection{``Adoption'' of supplemental legislation.}\label{reg_adopt}
Schools that fail to submit acceptable legislation will be have one item of supplemental 
legislation given to a randomly selected student as an adopted item as an 
alternative to dropping their delegation. (This ``no questions asked'' 
adoption policy is the \emph{exclusive relief} for extenuating circumstances 
that prevent timely submission of satisfactory legislation.) 
The assignment of legislation will be reported on the calendar.
Neither the adopted item nor the sponsor may be changed, though the
sponsor may decline to exercise the authorship privilege.

%A school that is not eligible to ``adopt'' legislation will be advised to
%reassign or drop its Congress entries. Regular tournament fees remain in effect.

%\vspace{.25in}

\subsection{Chamber Assignments and Calendars}\label{ch}

\subsubsection{}\label{ch_size}
All chambers are labeled by letter or number and may conduct themselves as Houses or Senates. 
The number of chambers will be determined by the entry count at the close of
registration. The preferred chamber size is 15--20, with an absolute maximum
of 22. Chambers \textbf{will not} be split or combined except in unusual
circumstances.

\subsubsection{}\label{ch_assign}
The Congress Coordinator will assign contestants to chambers; special requests
\textbf{will not} be entertained. Initial assignments are made by random draw;
entries (with names concealed) are then swapped to distribute legislation and
school delegations as evenly as possible. The Coordinator will assign seats
randomly; special seating requests \textbf{will not} be entertained except for
accessibility.

\subsubsection{}\label{ch_calendar}
The Congress Coordinator will prepare and publish a legislative calendar for
each chamber. Each approved B/R will be placed on the Primary Agenda in the
chamber in which its author is seated. If the author is not registered for
the tournament, the legislation will be placed at the bottom of a Primary
Agenda in a chamber selected at random.

The order of business will be set by blind draw, adjusted if necessary to
ensure rotation of schools. Each chamber's Secondary Agenda will consist
of items drawn from other chambers' Primary Agendas and supplemental items
prepared by the League. An ``adopted'' supplemental item will be placed
at the lowest permissible point on the Primary Agenda in its assigned 
sponsor's chamber.

\subsubsection{}\label{ch_posting}
Assignments, calendars, and seating charts will be posted to Tabroom.com and the
Congress site as soon as possible after the close of registration. \emph{However,
students \textbf{should not wait} for chamber assignments to begin their research!}


\subsection{Session Procedures}\label{sess}

\subsubsection{}\label{sess_opening}
Before Session 1, the Congress Coordinator will make announcements, answer
quesions, administer the Oath of Office, and appoint Temporary {\po}s. In
chambers, the Temporary POs will call the session to order, call the roll,
and conduct the first PO election.

\subsubsection{}\label{sess_floordebate}
The tournament consists two sessions of 150 minutes (2\nicefrac{1}{2} hours)
each. Session time includes preliminary business, floor debate, special orders,
and all recesses not established by rule.

\subsubsection{}\label{sess_pos}
{\po}s are elected by secret ballot at the start of each session. 
Nominations are taken from the floor and may be declined;
candidates \textbf{may not} self-nominate. 
All legislators are eligible to run for PO
but should consider their experience and familiarity with the rules 
before accepting a nomination.

After nominations are closed but before the vote is taken, 
each candidate may give a 15--30 second statement of her qualifications. 
All legislators may vote in the election, including the sitting PO. 
The vote should be counted by a judge or by two legislators
not in the running.

A \emph{simple majority} of ballots cast is required to elect a PO. If no
candidate receives a simple majority, the last-place candidate is eliminated and
a runoff is held among the survivors \textbf{unless} one of the following
applies:

\begin{enumerate}[noitemsep]
  \item If the bottom $n$ candidates have fewer \emph{combined} votes than
    the candidate immediately ahead of them (\emph{e.g.}, the vote is
    7--5--\textbf{2--1} or 6--5--\textbf{2--1--1}), all of the bottom
    $n$ (vote counts in \textbf{boldface}) are eliminated, and the runoff
    is held among the survivors.
  \item If there is a tie for last place and the ``combined votes'' rule
    does not apply (\emph{e.g.}, the vote is 8--\textbf{5--5} or
    7--\textbf{3--3--3}), a
    \emph{sudden death runoff} is held between the tied legislators only. The
    candidate with the fewest votes is eliminated, and another runoff is
    held among all the survivors.
  \item If \emph{all} candidates are tied, the balloting is repeated until
    the tie is broken or all but one candidate concedes or withdraws.
    At any time, the candidates may unanimously agree to cease balloting
    and substitute a mutually acceptable random or pseudorandom method,
    \emph{e.g.,} a coin toss.
\end{enumerate}

\subsubsection{}\label{sess_calendar}
The calendar \textbf{may not} be modified except as directed. However, the
chamber may use appropriate motions to address specific items without changing
the order. Items \textbf{must not} be added to the calendar without the
approval of the Tab Room.

Legislation \textbf{may} be tabled or postponed in Session 1 and taken up again
in Session 2.

\subsubsection{}\label{sess_recess}
No recess shall be longer than 10 minutes unless approved by the Tab Room. 
The motion to recess shall be \ooo\ during the following intervals:
\begin{itemize}[nolistsep,noitemsep]\small
	\item The first 30 minutes of each session
	\item The first 15 minutes after returning from a recess
	\item The last 30 minutes of each session, unless the chamber 
		has voted on every B/R on its Primary Agenda
\end{itemize}

\subsubsection{Special orders established by rule.}\label{sess_orders}
At the end of Session 1 (after 150 minutes of session time), the chamber
shall adjourn until 1:00 P.M. (or other established start time for Session 2).
Upon returning from adjournment, the Session 1 PO shall conduct the election
for the Session 2 PO. At the end of Session 2 (after 150 minutes of session
time), the chamber shall adjourn \emph{sine die}.


\subsection{Scoring and Tabulation}\label{scr}

\subsubsection{}\label{scr_judges}
Each chamber will have two judges per session to evaluate all speeches and the
performance of the PO. In the event of a serious procedural issue or a question
from the PO, either judge may advise the chamber or request the assistance of
the Tab Room. \emph{If the judges disagree on a question involving the rules,
the Congress Coordinator should be consulted.}

\subsubsection{}\label{scr_scale}
Speeches are scored on a scale from 1 (poor) to 6 (outstanding). The PO
receives two component scores on the 1--6 scale for a maximum of 12 points.
Higher scores are better, and the same score may be awarded to more than
one speaker or speech. The two judges' ballots should be averaged and rounded up
for purposes of awarding honor points. A ballot with evidence of a speech 
but no score may be recorded as the point maximum.

\subsubsection{}\label{scr_rank}
At the end of each session, each judge will select and rank
\emph{(independently and in secret)} the 8 legislators who, in their
holistic assessment, contributed the most to the session.
\textbf{The PO is eligible and MUST be considered.}
Judges may rank legislators from their own schools in any position.
An unranked legislator, or a legislator given a rank worse then eighth
preference, is assigned a rank of 9.

\subsubsection{Finishing order.}\label{scr_tiebreak}
The Tab Room will determine final placement based on low total ranks.
Ties will be broken by judge preference, then high total rank reciprocals, 
then number of first preferences, 
then ranks and reciprocals (best and worst rankings dropped).
Further ties are considered unbreakable.

Gavels are awarded to the {\po}s, and trophies are awarded to the top 3
finishers in each chamber.
