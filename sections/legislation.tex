\section{Legislation}

\subsection{Requirements for All Legislation}\label{leg}

\subsubsection{Threshold requirement.}\label{leg_threshold}
\emph{All Student Congress legislation must constitute a valid legislative
response to a timely and significant issue of national interest and public concern.}

\subsubsection{}\label{leg_frivolous}
Frivolous or stale legislation \textbf{will not} be considered at any
FGCCFL tournament. A B/R is frivolous if it fails to satisfy the threshold
requirement above, if it lacks a rational basis or affirms the status quo, 
if it has a patently unfair division of ground, 
or if the Congress Coordinator determines that the merits of debating the
B/R are outweighed by the legislation's disruptive potential.
A B/R is stale if it addresses a frequently debated topic and is not
\emph{especially} timely, significant, or novel.

\subsubsection{}\label{leg_recycled}
Students \textbf{may} present the same B/R more than once but
\textbf{may not} present an item they presented in any prior year. 
All legislation should be the original work of the author. 
A B/R may be informed by the work of other legislators and may address
similar subject matter, but it \textbf{must not} bear striking similarity
to the earlier work.

\emph{All legislation presented at Grand Finals must be new to FGCCFL.}

\subsubsection{}\label{leg_topicrecs}
The Congress Coordinator may publish a list of recommended topic areas for a
particular tournament and give higher priority on the calendar to B/R that
are germane to the recommended topic areas.

\subsubsection{Legislation templates.}\label{leg_templates}
FGCCFL has produced legislation templates to simplify drafting. These
templates may be downloaded from the FGCCFL Congress site. All legislation
\textbf{must} be written using the approved templates.

\vspace{1ex}
FGCCFL's current templates are compatible with Word, Pages, and Google Docs 
on every platform, including mobile devices, provided that the finished 
legislation is saved in Microsoft Word format.\footnote{This may require the 
use of an ``Export'' or ``Download as'' command depending on the software used.} 
\textbf{FGCCFL templates from prior years WILL NOT be accepted.} 
\vspace{1ex}

The legislation templates \textbf{are not} ``examples'' to be replicated in
a blank document. They are shells into which the text of the legislation is
to be typed, replacing the highlighted guide text. The template formatting 
\textbf{must not} be intentionally modified. For best results,
legislation text should be typed into the template rather than cut and
pasted.

\begin{framed}
  \centering\bfseries\itshape
  Legislation that does not use a current approved template in the
  manner directed WILL~NOT be considered at any FGCCFL tournament.
\end{framed}

\subsubsection{}\label{leg_formalities}
All legislation must bear a descriptive title (sufficient to permit
debate on title alone), capitalized according to MLA style, and a
``Respectfully submitted'' closing block with the author's name and
school. The maximum length is one side of a U.S. letter-size page.
\emph{Coaches should review all legislation for content, style,
mechanics, and suitability.}


\subsection{Bills}\label{b}

\subsubsection{}\label{b_overview}
A bill proposes a change in public policy and specifies the manner by
which the government will implement that change. If enacted, bills carry
the force of law. Bills \textbf{do not} explain \emph{why} a course of
action is desirable (that's the reason for the authorship speech); rather,
their focus is \emph{how} the proposed policy should be carried out.

\subsubsection{}\label{b_structure}
All bills must be written in the five-section format
(which may include lettered subsections) that has been approved by
FGCCFL and incorporated into the template:

\begin{enumerate}[noitemsep]
  \item \textbf{Section 1} states the action to be taken in one sentence, or
    as few sentences as possible. Provisions may not be incorporated by
    reference unless made explicit elsewhwere in the bill. Codification
    is neither required nor desirable in this format.
  \item \textbf{Section 2} clarifies terminology \emph{actually used
    elsewhere in the bill}. Only essential provisions should be included;
    words and phrases used for their plain meaning need not be defined in
    the bill. Definitions may invoke other laws but should
    not cite to sources other than federal statutes or regulations.
  \item \textbf{Section 3} identifies the federal agency or agencies that
    will be responsible for enforcing the bill 
    (see \S~\ref{b}\ref{b_implement}) and provides
    the enforcement mechanism, \emph{e.g.}, penalties, funding, 
    resolution of jurisdictional issues, etc.
  \item \textbf{Section 4} establishes when the bill takes effect. This may
    be a specific date, a period of time after passage, or immediately
    upon passage. Reference to a fiscal year, tax year, or school year is
    inappropriate. No other provisions should appear in this section.
  \item \textbf{Section 5} declares that all laws in conflict with the bill
    are null and void. The standard language in the template should not be
    modified.
\end{enumerate}

\noindent An enacting clause (\emph{``Be it enacted~\dots that:''}) appears
on the line before Section 1. Additional sections are not permitted.

\subsubsection{Enforcing agencies.}\label{b_implement}
Because FGCCFL Congress chambers are national legislative bodies, all
bills \textbf{must} have national scope. Use the following guidelines
to select an enforcing agency:

\begin{enumerate}[noitemsep]
  \item Bills should be implemented or enforced \emph{exclusively by
    agencies of the federal government, almost always in the Executive
    Branch}. Enforcement implies a grant of necessary rulemaking authority.
  \item When a specific agency (\emph{e.g.}, the Internal Revenue Service)
    is part of a Cabinet-level department (\emph{e.g.}, the Department of
    the Treasury), it is preferable to designate \textbf{only} the Cabinet-level
    agency as responsible for enforcing the bill. This is because federal
    regulations are generally issued by the Department, 
    in the name of the overseeing Cabinet secretary, 
    and not by the lower-level agency.
  \item Some agencies, \emph{e.g.}, the Social Security Administration,
    are not part of Cabinet departments. There are also agencies that
    function entirely within the White House, \emph{e.g.}, the Executive
    Office of the President. These may also be proper enforcing agencies.
  \item Congress itself does not enforce or implement laws (except for
    very rare exercises of Congressional prerogative\footnote{\emph{See}
    \textsc{U.S.~Const.} art.~I, \S~5.} that do not generally
    lend themselves to Student Congress), but Legislative Branch
    entities such as the GAO may occasionally be appropriate enforcing
    agencies.
  \item If a bill grants a private right of action (allowing a person or
    other private party to sue in federal court), the existing Section 3
    language should be replaced with a statement to the effect that
    \emph{``The district courts shall have jurisdiction over all cases
    arising under the provisions of this bill,''} followed by a statement
    of available remedies.
  \item Congressional oversight of the Executive Branch, routine appropriation
    of funds, and adjudication in the courts of matters brought by the
    government are not ``enforcement'' and do not need to be stated in the bill.
  \item \emph{Please ensure that the enforcing agency actually exists.}
    (If the bill creates a new agency, ensure that the parent agency exists.)
    Legislation that invokes the ``Department of Corrections,'' the
    ``Department of Health,'' the ``Department of Foreign Affairs,'' or
    similar nonexistent agencies will not be accepted for debate.
\end{enumerate}

\subsubsection{Federalism and enumerated powers.}\label{b_fed}
Bills involving matters traditionally handled at the state or local level
\textbf{must} address jurisdictional concerns. This could mean limiting the
bill's applicability to cases affecting interstate commerce, imposing
conditions for state or local entities to receive federal funds, or
structuring the bill so that it invokes some other provision of
Article I, Section 8.

(In general, bills that make an effort to recognize federalism
concerns will be given the benefit of the doubt.
The extent to which a bill addresses these concerns
and the legitimacy of the means used are legitimate issues to be raised
in floor debate.)


\subsection{Resolutions and Constitutional Amendments}\label{r}

\subsubsection{}\label{r_overview}
A resolution expresses a conviction or makes a suggestion about a current
issue. Resolutions do not carry the force of law; rather, passage of a
resolution means that the Congress endorses the position the resolution
takes.\footnote{\fnresmpx} Resolutions explain \emph{why} a position
deserves official support, but they \textbf{do not} prescribe \emph{how}
the government will implement the proposal.

\subsubsection{}\label{r_structure}
All resolutions must be written in the simple resolution format that is
incorporated in the FGCCFL template. This format consists of a series of
one-sentence \emph{``whereas'' clauses} (at least 3 recommended, except
for constitutional amendments) that make an argument for the resolution, followed
by one or more one-sentence \emph{resolving clauses} that state the
conviction or suggestion.

\subsubsection{}\label{r_implement}
Because resolutions do not carry the force of law, they may be directed toward
federal, state, local, foreign, corporate, or other entities' conduct. The
propriety of addressing a particular party is a legitimate issue to be raised in
floor debate.

\subsubsection{Specificity requirement.}\label{r_specificity}
To be debatable, a resolution \textbf{must} state a specific suggestion
and \textbf{may not} simply call for ``reform,'' ``improvement,''
``modification,'' etc. (\emph{How} should the status quo be reformed or modified?)
Calling for \emph{repeal} of an existing law or policy will generally satisfy
the specificity requirement.
Moreover, a resolution may not simply call for passage of a particular bill,
although it may call for adoption of the bill's policies, which are then set
forth in the resolving clauses.

\subsubsection{Constitutional amendments.}\label{r_constamend}
Because constitutional amendments do not carry the force of law unless ratified
by the states, they are treated as resolutions. FGCCFL provides a special
version of the simple resolution template for constitutional amendments which
incorporates widely accepted amendment language and formatting. In general,
legislators need only add the text of their amendment to Section 1 (adding
other sections if necessary) and select the ratification method (legislatures
or conventions); they may optionally change or remove the seven-year
ratification window.

\emph{Adoption of a resolution proposing a constitutional
amendment requires a \nicefrac{2}{3} vote.}
